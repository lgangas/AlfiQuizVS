﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using Entidades;
using Datos;

namespace Logica
{
    public class Promocion_manager
    {
        Promocio_DAL metodo = new Promocio_DAL();

        public bool crearPromocion(Promocion prom)
        {
            return metodo.crearPromocion(prom);
        }

        public JObject comprarPromocion(JObject jObject)
        {
            return metodo.comprarPromocion(jObject);
        }

        public List<Promocion> listarPromocion(int usu)
        {
            return metodo.listarPromocion(usu);
        }

        public List<Promocion> listarPromocionGeneral()
        {
            return metodo.listarPromocionGeneral();
        }

        public String prueba()
        {
            return metodo.prueba();
        }

    }
}
