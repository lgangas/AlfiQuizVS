﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Entidades;
using Datos;

namespace Logica
{
    public class Encuesta_manager
    {
        Encuesta_DAL metodo = new Encuesta_DAL();

        public string crearEncuesta(JObject jObject)
        {
            return metodo.crearEncuesta(jObject);
        }

        public JArray listarEncuestaPersona()
        {
            return metodo.listarEncuestaPersona();
        }

        public JObject obtenerPreguntar(JObject jObject)
        {
            return metodo.obtenerPreguntar(jObject);
        }

        public JObject hacerEncuesta(JObject jObject)
        {
            return metodo.hacerEncuesta(jObject);
        }
    }
}
