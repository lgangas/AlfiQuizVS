﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Datos;
using Entidades;

namespace Logica
{
    public class Usuario_manager
    {
        Usuario_DAL metodo = new Usuario_DAL();

        public Usuario ingresar(Usuario usu)
        {
            return metodo.ingresar(usu);
        }

        public UsuarioPersona buscarUsuarioPersona(int id)
        {
            return metodo.buscarUsuarioPersona(id);
        }

        public UsuarioEmpresa buscarUsuarioEmpresa(int id)
        {
            return metodo.buscarUsuarioEmpresa(id);
        }

        public bool registrarUsuarioPersona(UsuarioPersona usu)
        {
            return metodo.registrarUsuarioPersona(usu);
        }

        public bool registrarUsuarioEmpresa(UsuarioEmpresa usu)
        {
            return metodo.registrarUsuarioEmpresa(usu);
        }

        public string prueba(int id)
        {
            return metodo.prueba(id);
        }
    }
}
