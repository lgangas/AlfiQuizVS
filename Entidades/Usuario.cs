﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Referencias
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entidades
{
    public class Usuario
    {
        [DisplayName("Codigo usuario")]
        public int codigo { get; set; }
        [DisplayName("Correo")]
        [Required(ErrorMessage ="Este campo es necesario.")]
        [EmailAddress(ErrorMessage ="Formato de email invalido.")]
        public string correo { get; set; }
        [DisplayName("Clave")]
        [Required(ErrorMessage = "Este campo es necesario.")]
        public string clave { get; set; }
        public int tipo { get; set; }
        public int estado_registro { get; set; }
    }
}
