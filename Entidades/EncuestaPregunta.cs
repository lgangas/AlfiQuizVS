﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Anotaciones
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entidades
{
    class EncuestaPregunta
    {
        [DisplayName("Codigo encuesta pregunta")]
        public int codigo_encuesta_pregunta { get; set; }
        [DisplayName("Codigo encuesta")]
        public int codigo_encuesta { get; set; }
        [DisplayName("Pregunta")]
        public string pregunta { get; set; }
        [DisplayName("Numero orden")]
        public int numero_orden { get; set; }
        [DisplayName("Estado registro")]
        public int estado_registro { get; set; }
    }
}
