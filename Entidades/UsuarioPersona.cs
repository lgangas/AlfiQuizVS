﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Anotaciones
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entidades
{
    public class UsuarioPersona
    {
        [DisplayName("Codigo usuario persona")]
        public int codigo_usuario_persona { get; set; }
        [DisplayName("Dni")]
        public string dni { get; set; }
        [DisplayName("Nombre")]
        public string nombre { get; set; }
        [DisplayName("Apellido paterno")]
        public string apellido_paterno { get; set; }
        [DisplayName("Apellido materno")]
        public string apellido_materno { get; set; }
        [DisplayName("Correo")]
        public string correo { get; set; }
        [DisplayName("Clave")]
        public string clave { get; set; }
        [DisplayName("Puntaje bonificacion")]
        public int puntaje_bonificacion { get; set; }
        [DisplayName("Estado registro")]
        public int estado_registro { get; set; }
    }
}
