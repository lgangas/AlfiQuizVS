﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Anotaciones
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entidades
{
    class CompraPromocion
    {
        [DisplayName("Codigo compra promocion")]
        public int codigo_compra_promocion { get; set; }
        [DisplayName("Codigo promocion")]
        public int codigo_promocion { get; set; }
        [DisplayName("Codigo usuario persona")]
        public int codigo_usuario_persona { get; set; }
        [DisplayName("Estado registro")]
        public int estado_registro { get; set; }
    }
}
