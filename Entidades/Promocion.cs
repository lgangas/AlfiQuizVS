﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
//Anotaciones
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entidades
{
    public class Promocion
    {
        [DisplayName("Codigo promocion")]
        public int codigo_promocion { get; set; }
        [DisplayName("Codigo usuario empresa")]
        public int codigo_usuario_empresa { get; set; }
        [DisplayName("Titulo")]
        public string titulo { get; set; }
        [DisplayName("Descripcion")]
        public string descripcion { get; set; }
        [DisplayName("Imagen")]
        public string imagen { get; set; }
        [DisplayName("Bonificacion pago")]
        public int bonificacion_pago { get; set; }
        [DisplayName("Fecha inicio")]
        public string fecha_inicio { get; set; }
        [DisplayName("Fecha fin")]
        public string fecha_fin { get; set; }
        [DisplayName("Empresa")]
        public string empresa { get; set; }
        [DisplayName("Estado registro")]
        public int estado_registro { get; set; }
        [DataType(DataType.Html)]
        public string Caption { get; set; }
        [DisplayName("Imagen")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase ImageUpload { get; set; }
    }
}
