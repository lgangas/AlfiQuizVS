﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Anotaciones
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entidades
{
    class EncuestaUsuario
    {
        [DisplayName("Codigo encuesta usuario")]
        public int codigo_encuesta_usuario { get; set; }
        [DisplayName("Codigo encuesta")]
        public int codigo_encuesta { get; set; }
        [DisplayName("Codigo usuario persona")]
        public int codigo_usuario_persona { get; set; }
        [DisplayName("Estado registro")]
        public int estado_registro { get; set; }
    }
}
