﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Anotaciones
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entidades
{
    public class UsuarioEmpresa
    {
        [DisplayName("Codigo usuario empresa")]
        public int codigo_usuario_empresa { get; set; }
        [DisplayName("Ruc")]
        public string ruc { get; set; }
        [DisplayName("Nombre")]
        public string nombre { get; set; }
        [DisplayName("Correo")]
        public string correo { get; set; }
        [DisplayName("Clave")]
        public string clave { get; set; }
        [DisplayName("Estado registro")]
        public int estado_registro { get; set; }
    }
}
