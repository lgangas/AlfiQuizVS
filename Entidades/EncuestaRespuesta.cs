﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Anotaciones
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entidades
{
    class EncuestaRespuesta
    {
        [DisplayName("Codigo encuesta respuesta")]
        public int codigo_encuesta_repuesta { get; set; }
        [DisplayName("Codigo encuesta alternativa")]
        public int codigo_encuesta_alternativa { get; set; }
        [DisplayName("Estado registro")]
        public int estado_registro { get; set; }
    }
}
