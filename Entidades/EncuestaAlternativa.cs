﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Anotaciones
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entidades
{
    class EncuestaAlternativa
    {
        [DisplayName("Codigo encuesta alternativa")]
        public int codigo_encuesta_alternativa { get; set; }
        [DisplayName("Codigo encuesta pregunta")]
        public int codigo_encuesta_pregunta { get; set; }
        [DisplayName("Alternativa")]
        public string alternativa { get; set; }
        [DisplayName("Numero orden")]
        public int numero_orden { get; set; }
        [DisplayName("Texto")]
        public string texto { get; set; }
        [DisplayName("Estado registro")]
        public int estado_registro { get; set; }
    }
}
