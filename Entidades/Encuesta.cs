﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Anotaciones
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Entidades
{
    public class Encuesta
    {
        [DisplayName("Codigo encuesta")]
        public int codigo_encuesta { get; set; }
        [DisplayName("Codigo usuario empresa")]
        public int codigo_usuario_empresa { get; set; }
        [DisplayName("Nombre")]
        public string nombre { get; set; }
        [DisplayName("Fecha inicio")]
        public string fecha_inicio { get; set; }
        [DisplayName("Fecha fin")]
        public string fecha_fin { get; set; }
        [DisplayName("Token")]
        public string token { get; set; }
        [DisplayName("Limite registro")]
        public int limite_registro { get; set; }
        [DisplayName("Bonificacion")]
        public int bonificacion { get; set; }
        [DisplayName("Estado registro")]
        public int estado_registro { get; set; }
    }
}
