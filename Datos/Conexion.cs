﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Referencias
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Datos
{
    public class Conexion
    {
        SqlConnection connection;
        public SqlConnection Conectar()
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString);
            return connection;
        }
    }
}
