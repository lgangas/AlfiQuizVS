﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Entidades;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Datos
{
    public class Encuesta_DAL
    {

        SqlConnection connection;
        SqlDataAdapter comando;
        SqlDataReader dr;
        SqlCommand cmd;
        String errores;
        Conexion cn = new Conexion();

        public string crearEncuesta(JObject jObject)
        {
            string respuesta = "";
            connection = cn.Conectar();
            //SqlTransaction tr = connection.BeginTransaction();
            try
            {                
                cmd = new SqlCommand("usp_crear_encuesta", connection);
                //cmd = new SqlCommand("usp_crear_encuesta", connection,tr);
                cmd.CommandType = CommandType.StoredProcedure;

                string token = (string)jObject["token"];

                if (token.Equals("0"))
                {
                    token = Get8CharacterRandomString();
                }

                //parametros
                cmd.Parameters.AddWithValue("usu", (int)jObject["usuario"]);
                cmd.Parameters.AddWithValue("nom", (string)jObject["nombreEncuesta"]);
                cmd.Parameters.AddWithValue("fini", (string)jObject["fechaInicio"]);
                cmd.Parameters.AddWithValue("ffin", (string)jObject["fechaFin"]);
                cmd.Parameters.AddWithValue("token", (string)jObject["token"]);
                cmd.Parameters.AddWithValue("lim", (int)jObject["limite"]);
                cmd.Parameters.AddWithValue("bon", (int)jObject["bonificacion"]);

                connection.Open();
                Int32 codigoEncuesta = (Int32)cmd.ExecuteScalar();

                if (codigoEncuesta > 0)
                {
                    JArray jPregunta = (JArray)jObject["preguntas"];
                    int sizePregunta = jPregunta.Count;

                    for(int i = 0; i < sizePregunta; i++)
                    {
                        cmd = new SqlCommand("usp_crear_pregunta_encuesta",connection);
                        //cmd = new SqlCommand("usp_crear_pregunta_encuesta",connection,tr);
                        cmd.CommandType = CommandType.StoredProcedure;

                        int tipoPregunta = (int)jPregunta[i]["tipoPregunta"];
                        //parametros
                        cmd.Parameters.AddWithValue("cod_enc", codigoEncuesta);
                        cmd.Parameters.AddWithValue("preg", (string)jPregunta[i]["pregunta"]);
                        cmd.Parameters.AddWithValue("num", i+1);
                        cmd.Parameters.AddWithValue("alt", tipoPregunta);

                        Int32 codigoPregunta = (Int32)cmd.ExecuteScalar();

                        if(tipoPregunta == 0)
                        {
                            JArray jAlternativas = (JArray)jPregunta[i]["alternativas"];
                            int sizeAlternativa = jAlternativas.Count;

                            for(int j = 0; j < sizeAlternativa; j++)
                            {
                                cmd = new SqlCommand("usp_crear_alternativa_encuesta", connection);
                                cmd.CommandType = CommandType.StoredProcedure;

                                //parametros
                                cmd.Parameters.AddWithValue("enc_preg", codigoPregunta);
                                cmd.Parameters.AddWithValue("num", j+1);
                                cmd.Parameters.AddWithValue("tex",(string) jAlternativas[j]);

                                cmd.ExecuteNonQuery();
                                
                            }
                        }
                        respuesta = "success";
                    }
                }
                else
                {
                    //tr.Rollback();
                }
            }
            catch (Exception ex)
            {
                errores = ex.Message;
                //tr.Rollback();
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }
            return null;
        }

        public JObject hacerEncuesta(JObject jObject)
        {
            JObject jResponse = new JObject();
            connection = cn.Conectar();
            //SqlTransaction tr = connection.BeginTransaction();
            try
            {
                JArray jRespuestas = (JArray)jObject["respuestas"];
                int sizeRespuestas = jRespuestas.Count;
                connection.Open();
                for (int i=0; i < sizeRespuestas; i++)
                {
                    cmd = new SqlCommand("usp_hacer_encuesta", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("preg", (int)jRespuestas[i]["codigo_encuesta_pregunta"]);
                    cmd.Parameters.AddWithValue("resp", (string)jRespuestas[i]["respuesta"]);

                    cmd.ExecuteNonQuery();
                }

                cmd = new SqlCommand("usp_editar_puntos_persona", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("usu", (int)jObject["codigo_usuario_persona"]);
                cmd.Parameters.AddWithValue("punt", (int)jObject["puntos"]);

                cmd.ExecuteNonQuery();

                jResponse.Add("msg","Exitos, obtuviste "+ (int)jObject["puntos"] + ".");
                jResponse.Add("status",true);
            }
            catch (Exception ex)
            {
                errores = ex.Message;
                jResponse.Add("msg", errores);
                jResponse.Add("status", false);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }
            return jResponse;
        }

        public string Get8CharacterRandomString()
        {
            string path = Path.GetRandomFileName();
            path = path.Replace(".", ""); // Remove period.
            return path.Substring(0, 8);  // Return 8 character string
        }

        public JArray listarEncuestaPersona()
        {
            JArray jArray = new JArray();
            try
            {
                connection = cn.Conectar();
                cmd = new SqlCommand("usp_listar_encuesta_general", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                dr = null;
                connection.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    JObject obj = new JObject();
                    obj["codigo_encuesta"] = Convert.ToInt32(dr["codigo_encuesta"]);
                    obj["codigo_usuario_empresa"] = Convert.ToInt32(dr["codigo_usuario_empresa"]);
                    obj["encuesta"] = Convert.ToString(dr["encuesta"]);
                    obj["empresa"] = Convert.ToString(dr["empresa"]);
                    obj["token"] = Convert.ToString(dr["token"]);
                    obj["bonificacion"] = Convert.ToInt32(dr["bonificacion"]);
                    obj["cantidad_pregunta"] = Convert.ToInt32(dr["cantidad_pregunta"]);

                    jArray.Add(obj);
                }

            }
            catch (Exception ex)
            {
                errores = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }

            return jArray;
        }

        public JObject obtenerPreguntar(JObject jObject)
        {
            JObject obj = new JObject();
            JArray jArray = new JArray();

            try
            {
                connection = cn.Conectar();
                cmd = new SqlCommand("usp_obtener_pregunta", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //parametros
                cmd.Parameters.AddWithValue("enc", (int)jObject["encuesta"]);
                cmd.Parameters.AddWithValue("ord", (int)jObject["orden"]);

                dr = null;
                connection.Open();
                dr = cmd.ExecuteReader();
                dr.Read();
                
                obj["codigo_encuesta"] = Convert.ToInt32(dr["codigo_encuesta"]);
                obj["codigo_encuesta_pregunta"] = Convert.ToInt32(dr["codigo_encuesta_pregunta"]);
                obj["pregunta"] = Convert.ToString(dr["pregunta"]);
                obj["numero_orden"] = Convert.ToInt32(dr["numero_orden"]);
                obj["alternativa"] = Convert.ToInt32(dr["alternativa"]);

                dr.Close();
                if ((int)obj["alternativa"] == 0)
                {
                    cmd = new SqlCommand("usp_obtener_alternativa", connection);
                    cmd.CommandType = CommandType.StoredProcedure;

                    //parametros
                    cmd.Parameters.AddWithValue("preg", (int)obj["codigo_encuesta_pregunta"]);
                    dr = null;
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        JObject jAlt = new JObject();
                        jAlt["codigo_encuesta_alternativa"] = Convert.ToInt32(dr["codigo_encuesta_alternativa"]);
                        jAlt["texto"] = Convert.ToString(dr["texto"]);
                        jAlt["numero_orden"] = Convert.ToInt32(dr["numero_orden"]);

                        jArray.Add(jAlt);
                    }

                    obj["listarAlternativas"] = jArray;
                }
            }
            catch (Exception ex)
            {
                errores = ex.Message;
                obj.Add("msg",errores);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }
            return obj;
        }

    }
}
