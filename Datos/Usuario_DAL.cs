﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Referencias
using Entidades;
using System.Data;
using System.Data.SqlClient;


namespace Datos
{
    public class Usuario_DAL
    {
        SqlConnection connection;
        SqlDataAdapter comando;
        SqlDataReader dr;
        SqlCommand cmd;
        String errores;
        Conexion cn = new Conexion();

        public Usuario ingresar(Usuario usu)
        {
            Usuario usuario = new Usuario();

            try
            {
                connection = cn.Conectar();
                cmd = new SqlCommand("usp_login_usuario",connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //parametros
                cmd.Parameters.AddWithValue("email",usu.correo);
                cmd.Parameters.AddWithValue("clave", usu.clave);

                dr = null;
                connection.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    usuario.codigo = Convert.ToInt32(dr["codigo"]);
                    usuario.tipo = Convert.ToInt32(dr["Tipo de usuario"]);
                }

            }catch(Exception es)
            {
                errores = es.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }

            return usuario;
        }

        public UsuarioPersona buscarUsuarioPersona(int codigo)
        {
            UsuarioPersona usuario = new UsuarioPersona();

            try
            {
                connection = cn.Conectar();
                cmd = new SqlCommand("usp_buscar_usuario_persona", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //parametros
                cmd.Parameters.AddWithValue("codigo",codigo);

                dr = null;
                connection.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    usuario.codigo_usuario_persona = Convert.ToInt32(dr["codigo_usuario_persona"]);
                    usuario.dni = Convert.ToString(dr["dni"]);
                    usuario.nombre = Convert.ToString(dr["nombre"]);
                    usuario.apellido_paterno = Convert.ToString(dr["apelllido_paterno"]);
                    usuario.apellido_materno = Convert.ToString(dr["apellido_materno"]);
                    usuario.correo = Convert.ToString(dr["correo"]);
                    usuario.clave = Convert.ToString(dr["clave"]);
                    usuario.puntaje_bonificacion = Convert.ToInt32(dr["puntaje_bonificacion"]);
                    usuario.estado_registro = Convert.ToInt32(dr["estado_registro"]);
                }

            } catch(Exception ex)
            {
                errores = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }

            return usuario;
        }
        public UsuarioEmpresa buscarUsuarioEmpresa(int codigo)
        {
            UsuarioEmpresa usuario = new UsuarioEmpresa();

            try
            {
                connection = cn.Conectar();
                cmd = new SqlCommand("usp_buscar_usuario_empresa", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //parametros
                cmd.Parameters.AddWithValue("codigo", codigo);

                dr = null;
                connection.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    usuario.codigo_usuario_empresa = Convert.ToInt32(dr["codigo_usuario_empresa"]);
                    usuario.ruc = Convert.ToString(dr["ruc"]);
                    usuario.nombre = Convert.ToString(dr["nombre"]);
                    usuario.correo = Convert.ToString(dr["correo"]);
                    usuario.clave = Convert.ToString(dr["clave"]);
                    usuario.estado_registro = Convert.ToInt32(dr["estado_registro"]);
                }

            }
            catch (Exception ex)
            {
                errores = ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }

            return usuario;
        }

        public bool registrarUsuarioPersona(UsuarioPersona usu)
        {

            try
            {
                connection = cn.Conectar();
                cmd = new SqlCommand("usp_registrar_usuario_persona", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //parametros
                cmd.Parameters.AddWithValue("dni", usu.dni);
                cmd.Parameters.AddWithValue("nom", usu.nombre);
                cmd.Parameters.AddWithValue("apepat", usu.apellido_paterno);
                cmd.Parameters.AddWithValue("apemat", usu.apellido_materno);
                cmd.Parameters.AddWithValue("email", usu.correo);
                cmd.Parameters.AddWithValue("clave", usu.clave);

                connection.Open();
                int resultado = cmd.ExecuteNonQuery();

                if(resultado > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                errores = ex.Message;
                return false;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }
        }

        public bool registrarUsuarioEmpresa(UsuarioEmpresa usu)
        {
            try
            {
                connection = cn.Conectar();
                cmd = new SqlCommand("usp_registrar_usuario_empresa", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //parametros
                cmd.Parameters.AddWithValue("ruc", usu.ruc);
                cmd.Parameters.AddWithValue("nom", usu.nombre);
                cmd.Parameters.AddWithValue("email", usu.correo);
                cmd.Parameters.AddWithValue("clave", usu.clave);

                connection.Open();
                int resultado = cmd.ExecuteNonQuery();

                if (resultado > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                errores = ex.Message;
                return false;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }
        }

        public string prueba(int id)
        {

            try
            {
                connection = cn.Conectar();
                connection.Open();
                return "success";
            }
            catch(Exception e)
            {
                return e.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
            }
        }

    }

    

}

