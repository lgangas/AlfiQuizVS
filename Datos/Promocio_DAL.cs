﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using Entidades;
using System.Data;
using System.Data.SqlClient;


namespace Datos
{
    public class Promocio_DAL
    {
        SqlConnection connection;
        SqlDataAdapter comando;
        SqlDataReader dr;
        SqlCommand cmd;
        String errores;
        Conexion cn = new Conexion();

        public JObject comprarPromocion(JObject jObject)
        {
            JObject jReturn = new JObject();
            try
            {
                connection = cn.Conectar();
                cmd = new SqlCommand("usp_comprar_promocion", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //parametros
                cmd.Parameters.AddWithValue("per", (int)jObject["usuario"]);
                cmd.Parameters.AddWithValue("prom", (int)jObject["promocion"]);
                cmd.Parameters.AddWithValue("punt", (int)jObject["puntos"]);


                connection.Open();
                int resultado = cmd.ExecuteNonQuery();

                if (resultado > 0)
                {
                    jReturn.Add("msg", "Compra exitosa.");
                }
                else
                {
                    jReturn.Add("msg", "Error al comprar.");
                }
            }
            catch (Exception ex)
            {
                errores = ex.Message;
                jReturn.Add("msg", "Error al comprar " + errores);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }
            return jReturn;
        }

        public bool crearPromocion(Promocion prom)
        {
            try
            {
                connection = cn.Conectar();
                cmd = new SqlCommand("usp_crear_promocion", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //parametros
                cmd.Parameters.AddWithValue("usu", prom.codigo_usuario_empresa);
                cmd.Parameters.AddWithValue("tit", prom.titulo);
                cmd.Parameters.AddWithValue("des", prom.descripcion);
                cmd.Parameters.AddWithValue("bono", prom.bonificacion_pago);
                cmd.Parameters.AddWithValue("fini", prom.fecha_inicio);
                cmd.Parameters.AddWithValue("ffin", prom.fecha_fin);
                cmd.Parameters.AddWithValue("img", prom.imagen);


                connection.Open();
                int resultado = cmd.ExecuteNonQuery();

                if (resultado > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                errores = ex.Message;
                return false;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }
        }

        public List<Promocion> listarPromocion(int usu)
        {
            List<Promocion> lista = new List<Promocion>();

            try
            {
                connection = cn.Conectar();
                cmd = new SqlCommand("usp_listar_promocion", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                //parametros
                cmd.Parameters.AddWithValue("usu", usu);

                dr = null;
                connection.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Promocion prom = new Promocion();
                    prom.codigo_promocion = Convert.ToInt32(dr["codigo_promocion"]);
                    prom.codigo_usuario_empresa = Convert.ToInt32(dr["codigo_usuario_empresa"]);
                    prom.titulo = Convert.ToString(dr["titulo"]);
                    prom.descripcion = Convert.ToString(dr["descripcion"]);
                    prom.bonificacion_pago = Convert.ToInt32(dr["bonificacion_pago"]);
                    prom.fecha_inicio = Convert.ToString(dr["fecha_inicio"]);
                    prom.fecha_fin = Convert.ToString(dr["fecha_fin"]);
                    prom.imagen = Convert.ToString(dr["imagen"]);
                    prom.estado_registro = Convert.ToInt32(dr["estado_registro"]);

                    lista.Add(prom);
                }

            }
            catch (Exception es)
            {
                errores = es.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }
            return lista;
        }

        public List<Promocion> listarPromocionGeneral()
        {
            List<Promocion> lista = new List<Promocion>();

            try
            {
                connection = cn.Conectar();
                cmd = new SqlCommand("usp_listar_promocion_general", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                dr = null;
                connection.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Promocion prom = new Promocion();
                    prom.codigo_promocion = Convert.ToInt32(dr["codigo_promocion"]);
                    prom.codigo_usuario_empresa = Convert.ToInt32(dr["codigo_usuario_empresa"]);
                    prom.empresa = Convert.ToString(dr["empresa"]);
                    prom.titulo = Convert.ToString(dr["titulo"]);
                    prom.descripcion = Convert.ToString(dr["descripcion"]);
                    prom.bonificacion_pago = Convert.ToInt32(dr["bonificacion_pago"]);
                    prom.fecha_inicio = Convert.ToString(dr["fecha_inicio"]);
                    prom.fecha_fin = Convert.ToString(dr["fecha_fin"]);
                    prom.imagen = Convert.ToString(dr["imagen"]);

                    lista.Add(prom);
                }

            }
            catch (Exception es)
            {
                errores = es.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
                connection.Dispose();
                cmd.Dispose();
            }
            return lista;
        }

        public string prueba()
        {

            connection = cn.Conectar();

            if(connection != null)
            {
                try
                {
                    connection.Open();
                    return "si abre la conexion";
                }
                catch(Exception e)
                {
                    return e.Message;
                }
                
            } else
            {
                return "no existe conexion";
            }

        }
    }
}
