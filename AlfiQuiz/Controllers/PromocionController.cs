﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Entidades;
using Logica;

namespace AlfiQuiz.Controllers
{
    public class PromocionController : Controller
    {
        // GET: Promocion

        Promocion_manager metodo = new Promocion_manager();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult listarPromocion()
        {
            if (Session["usuario"] != null)
            {
                UsuarioEmpresa usu = (UsuarioEmpresa)Session["usuario"];

                return View(metodo.listarPromocion(usu.codigo_usuario_empresa));
            }
            return null;            
        }

        public ActionResult crearPromocion()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View(new Promocion());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Promocion model)
        {
            var validImageTypes = new string[]
            {
                "image/gif",
                "image/jpeg",
                "image/pjpeg",
                "image/png"
            };

            if (model.ImageUpload == null || model.ImageUpload.ContentLength == 0)
            {
                ModelState.AddModelError("ImageUpload", "This field is required");
            }
                else if (!validImageTypes.Contains(model.ImageUpload.ContentType))
            {
                ModelState.AddModelError("ImageUpload", "Please choose either a GIF, JPG or PNG image.");
            }

            if (ModelState.IsValid)
            {

                if (model.ImageUpload != null && model.ImageUpload.ContentLength > 0)
                {
                    UsuarioEmpresa usu = (UsuarioEmpresa)Session["usuario"];
                    if (usu != null)
                    {
                        model.codigo_usuario_empresa = usu.codigo_usuario_empresa;
                        model.imagen = model.ImageUpload.FileName;

                        string uri = "H:\\Visual Studio\\AlfiQuiz\\ServicioRestPublishing\\App_Data\\img\\" + model.ImageUpload.FileName;
                        model.ImageUpload.SaveAs(uri);

                        if (metodo.crearPromocion(model))
                        {
                            return RedirectToAction("listarPromocion");
                        }
                    }     
                }
            }
            return RedirectToAction("Create");
        }

        [HttpPost]
        public ActionResult crearPromocion(Promocion prom)
        {
            if (ModelState.IsValid)
            {
                UsuarioEmpresa usu = (UsuarioEmpresa)Session["usuario"];
                if (usu!=null)
                {
                    prom.codigo_usuario_empresa = usu.codigo_usuario_empresa;
                    if (metodo.crearPromocion(prom))
                    {
                        return RedirectToAction("listarPromocion");
                    }
                }
            }
            return RedirectToAction("crearPromocion");
        }
        
    }
}