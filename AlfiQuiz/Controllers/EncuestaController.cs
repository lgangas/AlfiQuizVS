﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;
using System.Web.Script;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;



using Entidades;
using Logica;

namespace AlfiQuiz.Controllers
{
    public class EncuestaController : Controller
    {
        Encuesta_manager metodo = new Encuesta_manager();
        // GET: Encuesta
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult crearEncuesta()
        {
            return View();
        }

        [HttpPost]
        public ActionResult crearEncuesta(string json)
        {

            if (Session["usuario"] != null)
            {
                UsuarioEmpresa usu = (UsuarioEmpresa)Session["usuario"];
                JObject data = JObject.Parse(json);
                data.Add("usuario", usu.codigo_usuario_empresa);

                string respuesta = metodo.crearEncuesta(data);

                return View();
            }

            
            
            //var lista = JsonConvert.DeserializeObject<List<Encuesta>>(jarray);
            //foreach(Encuesta e in lista)
            //{
            //    int cod = e.codigo_encuesta;
            //}
            return Content("");
        }
    }
}