﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Entidades;
using Logica;

namespace AlfiQuiz.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Usuario

        Usuario_manager metodo = new Usuario_manager();
        

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ingresar()
        {
            return View();
        }

        public ActionResult cerrarSesion()
        {
            Session["usuario"] = null;
            return RedirectToAction("ingresar");
        }

        [HttpPost]
        public ActionResult ingresar(Usuario usu)
        {
            if (ModelState.IsValid)
            {
                Usuario usuResponse = metodo.ingresar(usu);
                if(usuResponse != null)
                {
                    int tipoUsuario = usuResponse.tipo;
                    if(tipoUsuario == 1)
                    {
                        return RedirectToAction("inicioUsuarioPersona",new { id = usuResponse.codigo});
                    } else
                    {
                        return RedirectToAction("inicioUsuarioEmpresa", new { id = usuResponse.codigo });
                    }
                }
            }
            return null;
        }

        public ActionResult inicioUsuarioPersona(int id)
        {
            UsuarioPersona usu = metodo.buscarUsuarioPersona(id);

            if (usu != null)
            {
                if (Session["usuario"] == null)
                {
                    Session["usuario"] = usu;
                    return View(usu);
                }
            }

            return RedirectToAction("ingresar");

        }

        public ActionResult inicioUsuarioEmpresa(int id)
        {
            UsuarioEmpresa usu = metodo.buscarUsuarioEmpresa(id);

            if (usu != null)
            {
                if (Session["usuario"] == null)
                {
                    Session["usuario"] = usu;
                    return View(usu);
                }
            }

            return RedirectToAction("ingresar");
        }

        public ActionResult registrarUsuarioPersona()
        {
            return View();
        }

        [HttpPost]
        public ActionResult registrarUsuarioPersona(UsuarioPersona usu)
        {
            if (ModelState.IsValid)
            {
                if (metodo.registrarUsuarioPersona(usu))
                {
                    return RedirectToAction("ingresar");
                }
            }
            return RedirectToAction("ingresar");
        }

        public ActionResult registrarUsuarioEmpresa()
        {
            return View();
        }

        [HttpPost]
        public ActionResult registrarUsuarioEmpresa(UsuarioEmpresa usu)
        {
            if (ModelState.IsValid)
            {
                if (metodo.registrarUsuarioEmpresa(usu))
                {
                    return RedirectToAction("ingresar");
                }
            }
            return RedirectToAction("ingresar");
        }
    }
}